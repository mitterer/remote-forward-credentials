# Example Installation Layout

This directory shows how an installation of `remote-forward-credentials` may
look like in a home-directory.

The targets of the symbolic links would need to be adapted to the real locations
of the referenced files.
